export interface SongPart {
  name: string;
  content: string;
  version: number;
  changedBy?: any;
}

export interface Phrase {
  text: string;
  author?: any;
}

export interface Song {
  parts: SongPart[];
  phrases: Phrase[];
  structure: string[];
}
