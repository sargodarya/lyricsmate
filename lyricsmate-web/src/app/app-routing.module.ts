import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditorComponent } from './modules/editor/components/editor/editor.component';

export enum RoutePaths {
  Editor = 'editor'
}

const routes: Routes = [
  {
    path: RoutePaths.Editor,
    component: EditorComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
