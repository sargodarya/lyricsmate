import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

export interface DatamuseWordsQuery {
  meansLike: string;
  soundsLike: string;
  spelledLike: string;
  vocabulary: string;
  topics: string[];
  leftContext: string;
  rightContext: string;
  maximum: string;
  antonym: string;
  trigger: string;
  synonym: string;
  metadata: string[];
  rhymes: string;
  queryEcho: keyof DatamuseWordsQuery;
}

export enum QueryParamMap {
  meansLike = 'ml',
  soundsLike = 'sl',
  spelledLike = 'sp',
  synonym = 'rel_syn',
  rhymes = 'rel_rhy'
}

export enum DatamuseMetadataFlag {
  Definitions = 'd',
  PartsOfSpeech = 'p',
  SyllableCount = 's',
  Pronunciation = 'r',
  WordFrequency = 'f'
}

@Injectable({
  providedIn: 'root'
})
export class DatamuseService {

  public constructor(private readonly httpClient: HttpClient) {}

  private readonly API_BASE_URL = 'https://api.datamuse.com';

  public words(datamuseQuery: Partial<DatamuseWordsQuery>): Observable<any> {
    const params = Object.keys(datamuseQuery).reduce((prev, key) => {
      if (QueryParamMap[key]) {
        prev[QueryParamMap[key]] = datamuseQuery[key];
      }

      return prev;
    }, {});

    return this.httpClient.get(`${this.API_BASE_URL}/words`, { params });
  }

}
