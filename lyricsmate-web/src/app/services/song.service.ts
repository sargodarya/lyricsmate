import { Injectable } from '@angular/core';
import { Song } from '../interfaces/song';
import {
  BehaviorSubject,
  Subject
} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SongService {

  private readonly songs: Song[] = [];

  private readonly activeSongSource: Subject<Song> = new BehaviorSubject(null);
  public readonly activeSong = this.activeSongSource.asObservable();

  constructor() {
    this.activeSongSource.next({
      parts: [
        {
          name: 'Verse 1',
          version: 1,
          content: 'I am a verse\nwith multiple lines'
        },
        {
          name: 'Verse 2',
          version: 1,
          content: 'I am another verse\nwith multiple lines'
        },
        {
          name: 'Verse 3',
          version: 1,
          content: 'Does it ever stop with these verses?'
        },
        {
          name: 'Chorus',
          version: 1,
          content: 'What the heck, this chorus is whack'
        },
        {
          name: 'Bridge',
          version: 1,
          content: 'Burning...'
        },
      ],
      phrases: [],
      structure: [
        'Verse 1',
        'Verse 2',
        'Chorus',
        'Verse 3',
        'Bridge',
        'Chorus',
        'Chorus'
      ]
    });
  }
}
