import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditorComponent } from './components/editor/editor.component';
import { PanelGroupComponent } from './components/panel-group/panel-group.component';
import { PanelStackComponent } from './components/panel-stack/panel-stack.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { RhymesComponent } from './components/rhymes/rhymes.component';
import { SynonymsComponent } from './components/synonyms/synonyms.component';
import { MoodsComponent } from './components/moods/moods.component';
import { StructureComponent } from './components/structure/structure.component';
import { PartsComponent } from './components/parts/parts.component';
import { TextEditorComponent } from './components/text-editor/text-editor.component';
import { UiModule } from '../ui/ui.module';
import { ReactiveFormsModule } from '@angular/forms';
import { TextFieldModule } from '@angular/cdk/text-field';

@NgModule({
  declarations: [
    EditorComponent,
    PanelGroupComponent,
    PanelStackComponent,
    RhymesComponent,
    SynonymsComponent,
    MoodsComponent,
    StructureComponent,
    PartsComponent,
    TextEditorComponent
  ],
  exports: [
    EditorComponent
  ],
  imports: [
    CommonModule,
    DragDropModule,
    UiModule,
    ReactiveFormsModule,
    TextFieldModule
  ]
})
export class EditorModule { }
