import { Injectable } from '@angular/core';
import { DatamuseService } from '../../../services/datamuse.service';

@Injectable({
  providedIn: 'root'
})
export class SynonymService {

  findSynonymFor(word) {
    return this.datamuseService.words({
      synonym: word
    });
  }

  constructor(private readonly datamuseService: DatamuseService) {
  }

}
