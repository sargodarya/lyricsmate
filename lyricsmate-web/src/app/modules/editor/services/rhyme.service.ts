import { Injectable } from '@angular/core';
import { DatamuseService } from '../../../services/datamuse.service';
import { Observable } from 'rxjs';

export interface Rhyme {
  word: string;
  score: number;
  numSyllables: number;
}

@Injectable({
  providedIn: 'root'
})
export class RhymeService {

  public constructor(private readonly datamuseService: DatamuseService) {
  }

  public findRhymeFor(word: string): Observable<Rhyme[]> {
    return this.datamuseService.words({
      rhymes: word
    });
  }
}
