import { Component } from '@angular/core';
import { RhymeService } from '../../services/rhyme.service';
import { FormControl } from '@angular/forms';
import {
  debounceTime,
  switchMap,
  tap
} from 'rxjs/operators';

@Component({
  selector: 'lm-rhymes',
  templateUrl: './rhymes.component.html',
  styleUrls: ['./rhymes.component.scss']
})
export class RhymesComponent {

  public inputControl = new FormControl('');

  public rhymes =
    this.inputControl.valueChanges
      .pipe(
        debounceTime(1000),
        tap(() => this.isLoading = true),
        switchMap((value) => this.rhymeService.findRhymeFor(value)),
        tap(() => this.isLoading = false)
      );

  public isLoading = false;

  constructor(private readonly rhymeService: RhymeService) { }

}
