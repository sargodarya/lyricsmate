import { Component } from '@angular/core';
import { SongService } from '../../../../services/song.service';
import {
  map
} from 'rxjs/operators';

@Component({
  selector: 'lm-structure',
  templateUrl: './structure.component.html',
  styleUrls: ['./structure.component.scss']
})
export class StructureComponent {

  public songStructure = this.songService.activeSong
    .pipe(
      map((song) => song.structure),
    );

  constructor(public readonly songService: SongService) {
  }
}
