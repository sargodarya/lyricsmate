import { Component } from '@angular/core';
import { SongService } from '../../../../services/song.service';
import { map } from 'rxjs/operators';
import { SongPart } from '../../../../interfaces/song';

@Component({
  selector: 'lm-text-editor',
  templateUrl: './text-editor.component.html',
  styleUrls: ['./text-editor.component.scss']
})
export class TextEditorComponent {
  public parts = this.songService.activeSong.pipe(
    map(song => song.structure.map((partName: string) => song.parts.find((part) => part.name === partName)))
  );

  public activePart: SongPart = null;

  constructor(public readonly songService: SongService) {}

  logEvent($event) {
    console.log($event);
    $event.preventDefault();
  }

  markPartAsActive($event) {
    this.activePart = null;
    console.log($event);
  }

  markPartAsInactive($event) {
    this.activePart = null;
  }

  selected(part: SongPart, $event: any) {
    console.log('Substring is:', part.content.substring($event.srcElement.selectionStart, $event.srcElement.selectionEnd));
    console.log($event);
  }
}
