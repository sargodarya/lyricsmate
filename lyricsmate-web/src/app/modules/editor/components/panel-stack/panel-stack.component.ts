import {
  Component,
  ElementRef,
  HostBinding,
  Input,
  OnInit,
  Renderer2,
  ViewContainerRef
} from '@angular/core';

export enum DockPosition {
  Left,
  Right
}

@Component({
  selector: 'lm-panel-stack',
  templateUrl: './panel-stack.component.html',
  styleUrls: ['./panel-stack.component.scss']
})
export class PanelStackComponent {
  @Input() public dockPosition: DockPosition;
  @Input() public isLast: boolean;

  @HostBinding('style.flexBasis') public width;
  @HostBinding('style.flexGrow') public flexGrow = 1;

  public isDragging = false;

  constructor(private elementRef: ElementRef) {}

  repositionBar($event) {
    const currentWidth = this.elementRef.nativeElement.offsetWidth;
    this.width = currentWidth + $event.distance.x + 'px';

    this.isDragging = false;
    this.flexGrow = 0;
    $event.source.reset();
  }

  startDrag() {
    this.isDragging = true;
  }

}
