import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelStackComponent } from './panel-stack.component';

describe('PanelStackComponent', () => {
  let component: PanelStackComponent;
  let fixture: ComponentFixture<PanelStackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanelStackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelStackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
