import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import {
  debounceTime,
  switchMap,
  tap
} from 'rxjs/operators';
import { RhymeService } from '../../services/rhyme.service';
import { SynonymService } from '../../services/synonym.service';

@Component({
  selector: 'lm-synonyms',
  templateUrl: './synonyms.component.html',
  styleUrls: ['./synonyms.component.scss']
})
export class SynonymsComponent {

  public inputControl: FormControl = new FormControl('');

  public synonyms =
    this.inputControl.valueChanges
      .pipe(
        debounceTime(1000),
        tap(() => this.isLoading = true),
        switchMap((value) => this.synonymService.findSynonymFor(value)),
        tap(() => this.isLoading = false)
      );

  public isLoading = false;

  constructor(private readonly synonymService: SynonymService) { }
}
