import { Component } from '@angular/core';
import {
  CdkDragDrop,
  moveItemInArray
} from '@angular/cdk/drag-drop';
import { PartsComponent } from '../parts/parts.component';
import { RhymesComponent } from '../rhymes/rhymes.component';
import { SynonymsComponent } from '../synonyms/synonyms.component';
import { MoodsComponent } from '../moods/moods.component';
import { TextEditorComponent } from '../text-editor/text-editor.component';
import { StructureComponent } from '../structure/structure.component';
import { PanelDescriptor } from '../../interfaces/panel-descriptor';
import { PanelType } from '../../enums/panel-type';

@Component({
  selector: 'lm-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent {
  public layout: PanelDescriptor[][][] = [
    [
      [
        {
          title: 'Rhymes',
          type: PanelType.Rhymes,
          component: RhymesComponent
        },
        {
          title: 'Synonyms',
          type: PanelType.Synonyms,
          component: SynonymsComponent
        },
        {
          title: 'Moods',
          type: PanelType.Moods,
          component: MoodsComponent
        }
      ]
    ],
    [
      [
        {
          title: 'Editor',
          type: PanelType.TextEditor,
          component: TextEditorComponent
        }],
    ],
    [
      [
        {
          title: 'Structure',
          type: PanelType.Structure,
          component: StructureComponent
        },
        {
          title: 'Parts',
          type: PanelType.Parts,
          component: PartsComponent
        }
        ]
    ]
  ];

  dropped($event: CdkDragDrop<any>) {
    if ($event.container !== $event.previousContainer) {

    } else {
      // moveItemInArray()
    }
  }

  trackByIndex(index: number) {
    return index;
  }
}
