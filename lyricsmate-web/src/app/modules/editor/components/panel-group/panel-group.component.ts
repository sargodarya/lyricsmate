import {
  AfterViewChecked,
  AfterViewInit,
  ApplicationRef,
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  ElementRef,
  EmbeddedViewRef,
  Injector,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { PanelDescriptor } from '../../interfaces/panel-descriptor';

@Component({
  selector: 'lm-panel-group',
  templateUrl: './panel-group.component.html',
  styleUrls: ['./panel-group.component.scss']
})
export class PanelGroupComponent implements AfterViewInit, OnInit {
  @Input() public panels: PanelDescriptor[] = [];

  @ViewChild('panelContent') public panelContent: ElementRef;

  public activePanel: PanelDescriptor = null;

  public componentReference: ComponentRef<any> = null;

  public collapsed = false;

  public constructor(
    private readonly componentFactoryResolver: ComponentFactoryResolver,
    private readonly injector: Injector,
    private readonly appRef: ApplicationRef
  ) {}

  public ngOnInit() {
    this.activePanel = this.panels[0];
  }

  public ngAfterViewInit() {
    this.setActivePanelContent();
  }

  public updateActivePanel() {
    if (this.panels.indexOf(this.activePanel) === -1) {
      this.setActivePanel(this.panels[0]);
    }
  }

  public setActivePanel(panelToActivate: PanelDescriptor) {
    this.activePanel = panelToActivate;
    this.setActivePanelContent();
  }

  public setActivePanelContent() {
    if (this.componentReference) {
      this.appRef.detachView(this.componentReference.hostView);
      this.componentReference.destroy();
    }

    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.activePanel.component);
    const componentRef = componentFactory.create(this.injector);

    this.appRef.attachView(componentRef.hostView);
    this.panelContent.nativeElement.appendChild((componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0]);
    this.componentReference = componentRef;
  }
}
