import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { SongService } from '../../../../services/song.service';
import { SongPart } from '../../../../interfaces/song';
import { Observable } from 'rxjs';

interface ExtendedSongPart extends SongPart {
  wordCount: number;
  lineCount: number;
  characters: number;
}

@Component({
  selector: 'lm-parts',
  templateUrl: './parts.component.html',
  styleUrls: ['./parts.component.scss']
})
export class PartsComponent {

  public songParts: Observable<ExtendedSongPart[]> = this.songService.activeSong
    .pipe(
      map((song) => song.parts.map((part) =>
        ({
          ...part,
          wordCount: part.content.split(' ').length,
          characters: part.content.split('').length,
          lineCount: part.content.split('\r\n').length
        }))
      )
    );

  constructor(public readonly songService: SongService) {
  }

}
