export enum PanelType {
  Rhymes,
  Synonyms,
  Moods,
  TextEditor,
  Structure,
  Parts
}
