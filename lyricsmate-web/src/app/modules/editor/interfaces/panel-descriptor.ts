import { PanelType } from '../enums/panel-type';

export interface PanelDescriptor {
  title: string;
  type: PanelType;
  component: any;
}
